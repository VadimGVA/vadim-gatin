import got from 'got';

export default class Client {
  /*
   * Метод получения экземпляра Got
   */
  static getInstance() {
    const customGot = got.extend({
      prefixUrl: process.env.STAND,
    });
    return customGot;
  }
}
