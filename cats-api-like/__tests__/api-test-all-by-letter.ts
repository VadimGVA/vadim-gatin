import Client from '../../dev/http-client';
import type {CatMinInfo, CatsList} from '../../dev/types';
import is from "@sindresorhus/is";
import string = is.string;


const HttpClient = Client.getInstance();

it('Получить список котов, сгруппированный по группам', async () => {
    const response = await HttpClient.get('core/cats/allByLetter?limit=2', {
        responseType: 'json',
    });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toEqual({

        count_all: expect.any(Number),
        count_output: expect.any(Number),
        groups: expect.arrayContaining([
            expect.objectContaining({
                cats: expect.arrayContaining([
                    expect.objectContaining({
                        "id": expect.any(Number),
                        "name":expect.any(String),
                        "gender":expect.any(String),
                        "likes":expect.any(Number),
                        "dislikes":expect.any(Number),
                        "count_by_letter":expect.any(String),
                        "description":expect.any(String)
                    }),
                ]),
                count_by_letter: expect.any(Number),
                count_in_group: expect.any(Number),
                title: expect.any(String),
            }),
        ]),
    });
});
